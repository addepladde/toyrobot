import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Glue class for the toy robot simulator. Accepts one command line parameter with the input file name.
 */
public class Game {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Provide an input filename");
            System.exit(1);
        }
        String filename = args[0];
        new Game(filename);
    }

    private Board board;
    private Robot robot;
    private Map<String, Runnable> commands;

    Game(String filename) {
        board = new Board();
        Scanner inputScanner = getScanner(filename);
        runMainLoop(inputScanner);
    }

    /**
     * Starts reading lines from the provided scanner and executes the commands
     * @param sc The scanner from which to read input
     */
    private void runMainLoop(Scanner sc) {
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] words = line.split(" ");

            if (words.length == 1) {
                // Don't do anything until a robot has been created with
                // the PLACE X,Y,F command
                if (robot == null) continue;

                Runnable r = commands.get(words[0]);
                if (r != null) {
                    r.run();
                }

            } else if (words.length == 2 && words[0].equals("PLACE")) {
                robot = createRobotFromInput(words[1]);

                // Check if robot creation failed for some reason
                if (robot != null) {
                    commands = createCommandsMapFor(robot);
                }
            }
        }
    }


    /**
     * Map the available command words to an action
     * @param robot The robot that is supposed to perform the action
     * @return A map containing the command keywords and the actions for the keywords
     */
    private Map<String, Runnable> createCommandsMapFor(Robot robot) {
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("MOVE", this::moveForward);
        commands.put("LEFT", robot::turnLeft);
        commands.put("RIGHT", robot::turnRight);
        commands.put("REPORT", () -> System.out.println(getReportString()));
        return commands;
    }

    private void moveForward() {
        if (checkLegalMoveForward(robot, board)) {
            robot.moveForward();
        }
    }

    boolean checkLegalMoveForward(Robot robot, Board board) {
        switch (robot.getDirection()) {
            case Direction.NORTH:
                return robot.getY() < board.getHeight() - 1;
            case Direction.EAST:
                return robot.getX() < board.getWidth() - 1;
            case Direction.SOUTH:
                return robot.getY() > 0;
            case Direction.WEST:
                return robot.getX() > 0;
        }

        return false;

    }

    /**
     * Creates a scanner that reads from a file
     * @param filename The name of the file from which to read
     * @return Open scanner that reads from the input file
     */
    private Scanner getScanner(String filename) {
        Scanner sc;
        try {
            sc = new Scanner(new File(filename));
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Couldn't find the file. Provide a correct filename");
        }
        return sc;
    }

    Robot createRobotFromInput(String coordinationsAndFacingDirection) {
        String[] values = coordinationsAndFacingDirection.split(",");

        int x = Integer.parseInt(values[0]);
        // Check valid X coordinate
        if (x < 0 || x > board.getWidth() - 1) {
            return null;
        }


        int y = Integer.parseInt(values[1]);
        // Check valid Y coordinate
        if (y < 0 || y > board.getHeight() - 1) {
            return null;
        }

        String directionString = values[2];
        int direction = Direction.getDirectionFromString(directionString);

        return new Robot(x, y, direction);
    }

    /**
     * Creates a string with the position and direction of the robot
     * @return String of the format X,Y,F where F is the direction the robot is facing
     */
    String getReportString() {
        // Capacity 9 since max length of output is 9 when direction is NORTH,
        // e.g. 1,1,NORTH
        StringBuilder stringBuilder = new StringBuilder(9);
        stringBuilder.append(robot.getX());
        stringBuilder.append(",");
        stringBuilder.append(robot.getY());
        stringBuilder.append(",");
        stringBuilder.append(Direction.getStringFromDirection(robot.getDirection()));
        return stringBuilder.toString();
    }


    Board getBoard() {
        return board;
    }

    Robot getRobot() {
        return robot;
    }
}
