import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for directions.
 */
public class Direction {

    // Prevent instantiation
    private Direction() {}

    static final int NORTH = 0;
    static final int EAST = 1;
    static final int SOUTH = 2;
    static final int WEST = 3;
    static final String KEYWORD_NORTH = "NORTH";
    static final String KEYWORD_EAST = "EAST";
    static final String KEYWORD_SOUTH = "SOUTH";
    static final String KEYWORD_WEST = "WEST";

    private static Map<String, Integer> stringToDirectionMap = new HashMap<>(4);

    static {
        stringToDirectionMap.put(KEYWORD_NORTH, Direction.NORTH);
        stringToDirectionMap.put(KEYWORD_EAST, Direction.EAST);
        stringToDirectionMap.put(KEYWORD_SOUTH, Direction.SOUTH);
        stringToDirectionMap.put(KEYWORD_WEST, Direction.WEST);
    }

    /**
     * Get the direction value for the string provided
     * @param direction String representation of a direction, e.g. "NORTH"
     * @return Integer representation of a direction
     */
    static int getDirectionFromString(String direction) {
        return stringToDirectionMap.get(direction);
    }

    /**
     * Get string representation of a direction
     * @param direction The direction represented as an integer
     * @return The string representation of the integer
     */
    static String getStringFromDirection(int direction) {
        for (Map.Entry<String, Integer> entry : stringToDirectionMap.entrySet()) {
            if (entry.getValue() == direction) {
                return entry.getKey();
            }
        }
        throw new IllegalArgumentException("Invalid direction passed: " + direction);
    }
}

