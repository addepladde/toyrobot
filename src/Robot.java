/**
 * Representation of a Toy Robot.
 */
public class Robot {

    private int x;
    private int y;
    private int direction;

    public Robot(int x, int y, int direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    /**
     * Moves the robot one step forward.
     */
    public void moveForward() {
        switch (direction) {
            case Direction.NORTH:
                this.y++;
                break;
            case Direction.EAST:
                this.x++;
                break;
            case Direction.SOUTH:
                this.y--;
                break;
            case Direction.WEST:
                this.x--;
                break;
        }
    }


    public void turnLeft() {
        this.direction = Math.floorMod(this.direction - 1, 4);
    }

    public void turnRight() {
        this.direction = Math.floorMod(this.direction + 1, 4);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDirection() {
        return direction;
    }
}
