/**
 * Class representing the board on which a robot is placed. Standard width and height of 5 if nothing else is provided.
 */
public class Board {

    private final int width;
    private final int height;

    public Board(int width, int height) {
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Dimensions of board were invalid, width: " + width + ", height: " + height);
        }
        this.width = width;
        this.height = height;
    }

    public Board() {
        this(5, 5);
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
