import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by apals on 2017-03-19.
 */
public class RobotTests {

    @Test
    public void testInitialization() {
        int x = 0;
        int y = 0;
        int dir = Direction.NORTH;

        Robot r = new Robot(x, y, dir);
        assertEquals(x, r.getX());
        assertEquals(y, r.getY());
        assertEquals(dir, r.getDirection());
    }

    @Test
    public void testMoveForwardNorth() {
        Robot robot = new Robot(0, 0, Direction.NORTH);
        robot.moveForward();
        assertEquals(robot.getX(), 0);
        assertEquals(robot.getY(), 1);
        assertEquals(robot.getDirection(), Direction.NORTH);
    }

    @Test
    public void testMoveForwardEast() {
        Robot robot = new Robot(0, 0, Direction.EAST);
        robot.moveForward();
        assertEquals(robot.getX(), 1);
        assertEquals(robot.getY(), 0);
        assertEquals(robot.getDirection(), Direction.EAST);
    }

    @Test
    public void testMoveForwardSouth() {
        Robot robot = new Robot(5, 5, Direction.SOUTH);
        robot.moveForward();
        assertEquals(robot.getX(), 5);
        assertEquals(robot.getY(), 4);
        assertEquals(robot.getDirection(), Direction.SOUTH);
    }

    @Test
    public void testMoveForwardWest() {
        Robot robot = new Robot(5, 5, Direction.WEST);
        robot.moveForward();
        assertEquals(robot.getX(), 4);
        assertEquals(robot.getY(), 5);
        assertEquals(robot.getDirection(), Direction.WEST);
    }

    @Test
    public void testTurnRight() {
        Robot robot = new Robot(5, 5, Direction.NORTH);
        assertEquals(robot.getDirection(), Direction.NORTH);

        robot.turnRight();
        assertEquals(robot.getDirection(), Direction.EAST);

        robot.turnRight();
        assertEquals(robot.getDirection(), Direction.SOUTH);

        robot.turnRight();
        assertEquals(robot.getDirection(), Direction.WEST);

        robot.turnRight();
        assertEquals(robot.getDirection(), Direction.NORTH);
    }

    @Test
    public void testTurnLeft() {
        Robot robot = new Robot(5, 5, Direction.NORTH);
        assertEquals(robot.getDirection(), Direction.NORTH);

        robot.turnLeft();
        assertEquals(robot.getDirection(), Direction.WEST);

        robot.turnLeft();
        assertEquals(robot.getDirection(), Direction.SOUTH);

        robot.turnLeft();
        assertEquals(robot.getDirection(), Direction.EAST);

        robot.turnLeft();
        assertEquals(robot.getDirection(), Direction.NORTH);
    }
}
