import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by apals on 2017-03-19.
 */
public class GameTests {

    @Test
    public void testReportString() {
        Game game = new Game("inputs/example.txt");
        String report = game.getReportString();
        assertEquals(report,"0,1,NORTH");

        game = new Game("inputs/example2.txt");
        report = game.getReportString();
        assertEquals(report,"0,0,WEST");

        game = new Game("inputs/example3.txt");
        report = game.getReportString();
        assertEquals(report,"3,3,NORTH");
    }


    @Test
    public void testCheckLegalMoveForward() {
        Game game = new Game("inputs/SWcornerFacingSouth.txt");
        Robot r = game.getRobot();
        Board b = game.getBoard();
        // Robot starts at 0,0, facing south. Should not be able to move
        assertFalse(game.checkLegalMoveForward(game.getRobot(), game.getBoard()));

        r.turnRight();
        // Facing west wall
        assertFalse(game.checkLegalMoveForward(game.getRobot(), game.getBoard()));

        r.turnRight();
        // Facing north
        assertTrue(game.checkLegalMoveForward(game.getRobot(), game.getBoard()));

        r.turnRight();
        // Facing east
        assertTrue(game.checkLegalMoveForward(game.getRobot(), game.getBoard()));

    }

    @Test
    public void testCommandsBeforePlace() {
        Game game = new Game("inputs/commandsBeforePlace.txt");
        String report = game.getReportString();
        assertEquals(report,"0,0,WEST");
    }

    @Test
    public void testInvalidPlacement() {
        Game game = new Game("inputs/invalidPlacement.txt");
        assertNull(game.getRobot());
    }

    @Test
    public void testInvalidPlacementWithMoves() {
        Game game = new Game("inputs/invalidPlacementWithMoves.txt");
        assertNull(game.getRobot());
    }

    @Test
    public void testDoublePlacement() {
        Game game = new Game("inputs/doublePlacement.txt");
        String expectedReport = "3,3,NORTH";
        assertEquals(game.getReportString(), expectedReport);
    }

    @Test
    public void testRobotCreation() {
        Game game = new Game("inputs/doublePlacement.txt");
        Robot r = game.createRobotFromInput("0,0,NORTH");
        assertEquals(r.getX(), 0);
        assertEquals(r.getY(), 0);
        assertEquals(r.getDirection(), Direction.NORTH);

        r = game.createRobotFromInput("-1,0,NORTH");
        assertNull(r);
    }

    @Test
    public void testMovingOutsideBoard() {
        Game game = new Game("inputs/movingOutsideBoard.txt");
        assertEquals(game.getRobot().getX(), 0);
        assertEquals(game.getRobot().getY(), 1);
        assertEquals(game.getRobot().getDirection(), Direction.NORTH);
    }
}
