import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by apals on 2017-03-19.
 */
public class DirectionTests {

    @Test
    public void testDirectionFromString() {
        int north = Direction.getDirectionFromString(Direction.KEYWORD_NORTH);
        int east = Direction.getDirectionFromString(Direction.KEYWORD_EAST);
        int south = Direction.getDirectionFromString(Direction.KEYWORD_SOUTH);
        int west = Direction.getDirectionFromString(Direction.KEYWORD_WEST);

        assertEquals(north, Direction.NORTH);
        assertEquals(east, Direction.EAST);
        assertEquals(south, Direction.SOUTH);
        assertEquals(west, Direction.WEST);
    }

    @Test
    public void testStringFromDirection() {
        String north = Direction.getStringFromDirection(Direction.NORTH);
        String east = Direction.getStringFromDirection(Direction.EAST);
        String south = Direction.getStringFromDirection(Direction.SOUTH);
        String west = Direction.getStringFromDirection(Direction.WEST);

        assertEquals(north, Direction.KEYWORD_NORTH);
        assertEquals(east, Direction.KEYWORD_EAST);
        assertEquals(south, Direction.KEYWORD_SOUTH);
        assertEquals(west, Direction.KEYWORD_WEST);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalDirectionInt() {
        Direction.getStringFromDirection(-1);
    }
}
