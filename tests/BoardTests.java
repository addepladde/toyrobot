import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by apals on 2017-03-19.
 */
public class BoardTests {

    @Test
    public void testCreationConditionsOfBoard() {
        int width = 1;
        int height = 2;
        Board b = new Board(width, height);
        assertEquals(b.getWidth(), width);
        assertEquals(b.getHeight(), height);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionForIllegalWidth() {
        int width = -1;
        int height = 2;
        Board b = new Board(width, height);
        assertEquals(b.getWidth(), width);
        assertEquals(b.getHeight(), height);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionForIllegalHeight() {
        int width = 10;
        int height = -2;
        Board b = new Board(width, height);
        assertEquals(b.getWidth(), width);
        assertEquals(b.getHeight(), height);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionForIllegalDimensions() {
        int width = -10;
        int height = -2;
        Board b = new Board(width, height);
        assertEquals(b.getWidth(), width);
        assertEquals(b.getHeight(), height);
    }
}
